module.exports = {
    semi: true,
    proseWrap: 'preserve',
    trailingComma: 'es5',
    useTabs:false,
    singleQuote: false,
    printWidth: 120,
    tabWidth: 2,
    arrowParens: 'always',
    endOfLine: 'auto',
    jsxBracketSameLine: true,
  }