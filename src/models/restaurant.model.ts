export enum TypeRestau {
  Asiatique = "Asiatique",
  FastFood = "FastFood",
  Burger = "Burger",
  kebab = "kebab",
  Others = "Others",
}

export enum LieuxRestau {
  Toulouse = "Toulouse",
  Lyon = "Lyon",
}

export enum NoteRestau {
  Un = "1/5",
  Deux = "2/5",
  Trois = "3/5",
  Quatre = "4/5",
  Cinq = "5/5",
}

export default class Restaurant {
  id: number;
  name: string;
  type: TypeRestau;
  lieux: LieuxRestau;
  note: NoteRestau;
  link: string;
  constructor(id: number, name: string, link: string) {
    this.id = id;
    this.name = name;
    this.type = TypeRestau.Others;
    this.note = NoteRestau.Un;
    this.lieux = LieuxRestau.Toulouse;
    this.link = link;
  }
}
