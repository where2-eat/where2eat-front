export interface UserMeService {
  id: number;
  email: string;
  username: string;
  role: UserRole;
}
export enum UserRole {
  USER = "user",
  MODERATOR = "modo",
  ADMIN = "admin",
}
