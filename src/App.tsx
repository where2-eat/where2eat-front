import { useSelector } from "react-redux";
import { HashRouter as Router, Route, Switch } from "react-router-dom";
import Dashboard from "./components/main-screen";
import PrivateRoute from "./PrivateRoute";
import { RootState } from "./store";
import Login from "./components/common/login";
import NavBar from "../src/components/common/nav-bar";
import PageNotFound from "./components/common/page-not-found";
import RestaurantAdd from "./components/restaurant/res-add";
import RestaurantScreen from "./components/restaurant.screen";
import RestaurantDetails from "./components/restaurant/res-details";
import { makeStyles } from "@material-ui/core";

export const App = () => {
  //Selectors
  const isLoggedIn = useSelector((state: RootState) => {
    return state.auth.isLoggedIn;
  });
  const classes = useStyles();

  return (
    <>
      <Router>
        <div className={classes.root}>
          <NavBar isAuthenticated={isLoggedIn} />
          <div>
            <Switch>
              <Route exact path={["/login"]} component={Login} />
              <PrivateRoute exact path={["/", "/dashboard"]} component={Dashboard} isAuthenticated={isLoggedIn} />
              <PrivateRoute exact path="/restaurant" component={RestaurantScreen} isAuthenticated={isLoggedIn} />
              <PrivateRoute exact path="/restaurant/add" component={RestaurantAdd} isAuthenticated={isLoggedIn} />
              <PrivateRoute exact path="/restaurant/:id" component={RestaurantDetails} isAuthenticated={isLoggedIn} />
              <PrivateRoute exact path={["*", "/*"]} component={PageNotFound} isAuthenticated={isLoggedIn} />
            </Switch>
          </div>
        </div>
      </Router>
    </>
  );
};

const useStyles = makeStyles((theme) => ({
  root: {
    height: "100vh",
    /*     backgroundColor: 'blue',
    [theme.breakpoints.up('sm')]: {
      backgroundColor: 'red',
    },
    [theme.breakpoints.up('md')]: {
      backgroundColor: 'green',
    },
    [theme.breakpoints.up('lg')]: {
      backgroundColor: 'orange',
    },
    [theme.breakpoints.up('xl')]: {
      backgroundColor: 'cyan',
    }, */
    "@media only screen and(-webkit-min-device-pixel:1.5), only screen and(-o-min-device-pixel:3/2), only screen and (min--moz-device-pixel-ratio:1.5), only screen and (min-device-pixel-ratio:1.5)":
      {
        body: {
          width: "100%",
          overflowX: "hidden",
        },
      },
  },
}));
