export const REGEX = {
    RESTAURANTNAME: new RegExp(/^((\w|\d).*){3,20}$/),
    RESTAURANTDESCRIPTION: new RegExp(/^((\w|\d).*){4,50}$/),
    EMAIL: new RegExp(
      /^(([^<>()[\].,;:\s@"]+(\.[^<>()[\].,;:\s@"]+)*)|(".+"))@(([^<>()[\].,;:\s@"]+\.)+[^<>()[\].,;:\s@"]{2,})$/
    ),
    PASSWORD: new RegExp(/^(\w|\d).*$/),
    FIRSTNAME: new RegExp(/^(\w|\d).*$/),
    LASTNAME: new RegExp(/^(\w|\d).*$/),
    ADDRESSLABEL: new RegExp(/^((\w|\d).*){4,12}$/),
    // eslint-disable-next-line
    URL: new RegExp(/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/),
  };