import React, { FunctionComponent, useEffect, useState } from "react";

const Clock: FunctionComponent = () => {
  const [currentDateTime, setCurrentDateTime] = useState<string>(new Date().toLocaleString());
  useEffect(() => {
    setInterval(() => {
      setCurrentDateTime(new Date().toLocaleString());
    }, 1000);
  });
  return <div>{currentDateTime}</div>;
};

export default Clock;
