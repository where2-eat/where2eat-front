import React, { FunctionComponent } from "react";
import { Link } from "react-router-dom";

const PageNotFound: FunctionComponent = () => {
  return (
    <div className="center">
      <h1>404h</h1>
      <Link to="/">Wayback</Link>
    </div>
  );
};

export default PageNotFound;
