/* eslint-disable react/prop-types */
import { Button, makeStyles, Typography } from "@material-ui/core";
import { FunctionComponent, useEffect, useState } from "react";
import { RouteComponentProps, useHistory } from "react-router-dom";
import Restaurant from "../../models/restaurant.model";
import RestaurantService from "../../store/services/restaurant-service";
import RestaurantForm from "./res-form";

type Params = { id: string };

const RestaurantDetails: FunctionComponent<RouteComponentProps<Params>> = ({ match }) => {
  const history = useHistory();
  const classes = useStyles();

  const [restaurant, setRestaurant] = useState<Restaurant>();

  const deleteRestaurant = (id: number) => {
    RestaurantService.deleteRestauByID(id);
    history.push(`/restaurant/`);
  };

  const goBack = () => {
    history.push("/restaurant/");
  };

  useEffect(() => {
    RestaurantService.getRestauByID(+match.params.id).then((restaurant) => setRestaurant(restaurant));
  }, [match.params.id]);
  return (
    <div className="row">
      <Typography className={classes.header} component="h1" variant="h5">
        Modify a resto
      </Typography>
      {restaurant ? <RestaurantForm restaurant={restaurant} isEditForm={true} /> : <> </>}
      <Button className={classes.submit} type="submit" variant="contained" color="primary" onClick={() => goBack()}>
        Retour
      </Button>
      <Button
        className={classes.delete}
        type="submit"
        variant="contained"
        onClick={() => deleteRestaurant(+match.params.id)}>
        Supprimer
      </Button>
    </div>
  );
};

export default RestaurantDetails;

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  submit: {
    margin: theme.spacing(3, 2, 1, 5),
    backgroundColor: "black",
    alignItems: "center",
    position: "relative",
    left: "38%",
  },
  delete: {
    margin: theme.spacing(3, 2, 1, 5),
    backgroundColor: "red",
    alignItems: "center",
    position: "relative",
    left: "38%",
  },
  header: {
    margin: theme.spacing(3, 2, 1, 5),
    alignItems: "center",
    position: "relative",
    left: "36%",
  },
}));
