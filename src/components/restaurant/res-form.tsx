/* eslint-disable react/prop-types */
import { Button, Container, makeStyles, MenuItem, Select, TextField } from "@material-ui/core";
import React, { FunctionComponent, useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import Restaurant, { LieuxRestau, NoteRestau, TypeRestau } from "../../models/restaurant.model";
import RestaurantService from "../../store/services/restaurant-service";
import * as Constants from "../../utils/constant";

type Props = {
  restaurant: Restaurant;
  isEditForm: boolean;
  handleClose?: VoidFunction;
};

const RestaurantForm: FunctionComponent<Props> = ({ restaurant, isEditForm, handleClose }) => {
  const history = useHistory();
  const classes = useStyles();

  const [name, setName] = useState<string>("");
  const [nameError, setNamekError] = useState<boolean>(false);
  const [type, setType] = useState<TypeRestau>(TypeRestau.Others);
  const [note, setNote] = useState<NoteRestau>(NoteRestau.Un);
  const [lieux, setLieux] = useState<LieuxRestau>(LieuxRestau.Toulouse);
  const [link, setLink] = useState<string>("");
  const [linkError, setLinkError] = useState<boolean>(false);

  const handleNameChange = (e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>): void => {
    const value: string = e.target.value;
    setName(value);
  };

  const handleTypeChange = (e: React.ChangeEvent<{ name?: string | undefined; value: unknown }>): void => {
    const value: any = e.target.value;
    setType(value);
  };
  const handleNoteChange = (e: React.ChangeEvent<{ name?: string | undefined; value: unknown }>): void => {
    const value: any = e.target.value;
    console.log(value);
    setNote(value);
  };
  const handleLieuxChange = (e: React.ChangeEvent<{ name?: string | undefined; value: unknown }>): void => {
    const value: any = e.target.value;
    setLieux(value);
  };
  const handleLinkChange = (e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>): void => {
    const value: string = e.target.value;
    const urlRegex: RegExpMatchArray | null = value.match(Constants.REGEX.URL);
    if (!urlRegex) {
      setLinkError(true);
    } else {
      setLink(value);
    }
  };

  const handleSubmit = (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
    e.preventDefault();
    restaurant.name = name;
    restaurant.type = type;
    restaurant.note = note;
    restaurant.lieux = lieux;
    restaurant.link = link;

    let canBeSend = true;
    if (restaurant.name.length < 3) {
      canBeSend = false;
      setNamekError(true);
    }
    if (canBeSend) {
      addRestau();
    }
  };

  const addRestau = () => {
    if (isEditForm) {
      RestaurantService.updateRestaurant(restaurant).then(() => history.push(`/restaurant`));
    } else {
      RestaurantService.addRestaurant(restaurant).then(() => (handleClose ? handleClose() : () => {}));
    }
  };

  useEffect(() => {
    if (isEditForm) {
      setName(restaurant.name);
      setType(restaurant.type);
      setNote(restaurant.note);
      setLieux(restaurant.lieux);
      setLink(restaurant.link);
    }
  }, [isEditForm, restaurant]);
  return (
    <Container component="main" maxWidth="xs">
      <form className={classes.form} noValidate>
        <TextField
          variant="outlined"
          margin="normal"
          required
          fullWidth
          id="name"
          label="name"
          name="name"
          autoFocus
          value={name}
          error={nameError}
          onChange={(e) => handleNameChange(e)}
        />
        <Select
          variant="outlined"
          style={{ marginTop: "5px", marginBottom: "5px" }}
          required
          fullWidth
          label="type"
          id="type"
          name="type"
          value={type}
          onChange={(e) => handleTypeChange(e)}>
          {Object.values(TypeRestau).map((value: string) => {
            return (
              <MenuItem key={value} value={value}>
                {value}
              </MenuItem>
            );
          })}
        </Select>
        <Select
          variant="outlined"
          style={{ marginTop: "5px", marginBottom: "5px" }}
          required
          fullWidth
          label="note"
          id="note"
          name="note"
          value={note}
          onChange={(e) => handleNoteChange(e)}>
          {Object.values(NoteRestau).map((value: string) => {
            return (
              <MenuItem key={value} value={value}>
                {value}
              </MenuItem>
            );
          })}
        </Select>
        <Select
          variant="outlined"
          required
          style={{ marginTop: "5px" }}
          fullWidth
          label="lieux"
          id="lieux"
          name="lieux"
          value={lieux}
          onChange={(e) => handleLieuxChange(e)}>
          {Object.values(LieuxRestau).map((value: string) => {
            return (
              <MenuItem key={value} value={value}>
                {value}
              </MenuItem>
            );
          })}
        </Select>

        <TextField
          variant="outlined"
          margin="normal"
          required
          fullWidth
          name="link"
          label="image link"
          type="link"
          value={link}
          error={linkError}
          onChange={(e) => handleLinkChange(e)}
        />
        <Button
          type="submit"
          fullWidth
          variant="contained"
          color="primary"
          className={classes.submit}
          onClick={(e) => handleSubmit(e)}>
          Envoyer
        </Button>
      </form>
    </Container>
  );
};

export default RestaurantForm;

const useStyles = makeStyles((theme) => ({
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
    backgroundColor: "green",
  },
}));
