import { Typography } from "@material-ui/core";
import React, { FunctionComponent, useState } from "react";
import Restaurant from "../../models/restaurant.model";
import RestaurantForm from "./res-form";

type Props = {
  handleClose: VoidFunction;
};

const RestaurantAdd: FunctionComponent<Props> = ({ handleClose }) => {
  const [id] = useState<number>(new Date().getTime());
  const [restaurant] = useState<Restaurant>(new Restaurant(id, " ", " "));

  return (
    <div className="row">
      <Typography component="h1" variant="h5">
        Create a resto
      </Typography>
      <RestaurantForm restaurant={restaurant} isEditForm={false} handleClose={handleClose} />
    </div>
  );
};

export default RestaurantAdd;
