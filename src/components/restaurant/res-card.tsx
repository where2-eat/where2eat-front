/* eslint-disable react/prop-types */
import { FunctionComponent } from "react";
import { makeStyles, Card, Theme, CardMedia, Grow, Typography, CardContent } from "@material-ui/core";
import Restaurant from "../../models/restaurant.model";
import { useHistory } from "react-router-dom";

type Props = {
  restaurant: Restaurant;
};

const RestaurantCard: FunctionComponent<Props> = ({ restaurant }): JSX.Element => {
  const classes = useStyles();
  const history = useHistory();

  const goToEquipe = (id: number) => {
    history.push(`/restaurant/${id}`);
  };

 /*  const tags = [
    "http://i.pravatar.cc/300?img=1",
    "http://i.pravatar.cc/300?img=2",
    "http://i.pravatar.cc/300?img=3",
    "http://i.pravatar.cc/300?img=4",
  ]; */

  return (
    <div onClick={() => goToEquipe(restaurant.id)}>
      <Grow in={true} timeout={1000}>
        <Card className={classes.CardUn}>
          <CardMedia className={classes.Media} image={restaurant.link} />
          <CardContent className={classes.MuiCardContent}>
            <Typography className={classes.heading} variant={"h6"} gutterBottom>
              {restaurant.name}
            </Typography>
            {/*           <Typography className={classes.subheading} variant={"caption"}>
           Coming soon :)
          </Typography> */}
          </CardContent>
        </Card>
      </Grow>
    </div>
  );
};

export default RestaurantCard;

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const useStyles = makeStyles((theme: Theme) => ({
  CardUn: {
    transition: "0.3s",
    width: "250px",
    heigh: "150px",
    margin: "15px",
    flex: 1,
    flexDirection: "row",
    boxShadow: "0 8px 40px -12px rgba(0,0,0,0.3)",
    "&:hover": {
      boxShadow: "0 16px 70px -12.125px rgba(0,0,0,0.3)",
    },
    [theme.breakpoints.down("sm")]: {
      width: "150px",
    },
    [theme.breakpoints.down("xs")]: {
      width: "75px",
    },
  },
  Media: {
    paddingTop: "56.25%",
    [theme.breakpoints.down("xs")]: {
      paddingTop: "100%",
    },
  },
  MuiCardContent: {
    textAlign: "left",
  },
  heading: {
    fontWeight: "bold",
    [theme.breakpoints.down("md")]: {
      fontSize: 12,
    },
    [theme.breakpoints.down("sm")]: {
      fontSize: 10,
    },
    [theme.breakpoints.down("xs")]: {
      fontSize: 10,
    },
  },
  subheading: {
    lineHeight: 1.8,
    [theme.breakpoints.down("sm")]: {
      fontSize: 7,
      display: "none",
    },
    [theme.breakpoints.down("xs")]: {
      display: "none",
    },
  },
  tags: {
    display: "inline-block",
    border: "2px solid white",
  },
}));
