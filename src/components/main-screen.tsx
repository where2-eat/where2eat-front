import { CardMedia, Container, makeStyles, Typography } from "@material-ui/core";
import React, { FunctionComponent } from "react";

const Dashboard: FunctionComponent = () => {
  const classes = useStyles();

  return (
    <Container>
      <Typography> Coming soon </Typography>
      <CardMedia className={classes.media} image="../assets/welcome.png" title="Welcome message" />
    </Container>
  );
};

export default Dashboard;
const useStyles = makeStyles(() => ({
  media: {
    height: 0,
    paddingTop: "56.25%", // 16:9
  },
}));
