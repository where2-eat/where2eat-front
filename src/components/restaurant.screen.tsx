import React, { FunctionComponent, useEffect, useState } from "react";
import Restaurant, { LieuxRestau } from "../models/restaurant.model";
import RestaurantService from "../store/services/restaurant-service";
import { Button, Dialog, Grid, makeStyles, MenuItem, Select, Theme } from "@material-ui/core";
import AddCircleIcon from "@material-ui/icons/AddCircle";
import RestoreIcon from "@material-ui/icons/Restore";
import ShuffleIcon from "@material-ui/icons/Shuffle";
import RestaurantCard from "./restaurant/res-card";
import RestaurantAdd from "./restaurant/res-add";

const RestaurantScreen: FunctionComponent = () => {
  //const classes = useStyles();
  const [currentRestaurant, setCurrentRestaurant] = useState<Restaurant[]>([]);
  const [displayedRestaurant, setDisplayedRestaurant] = useState<Restaurant[]>([]);
  const [selectedLieux, setSelectedLieux] = useState<string>("Toulouse");
  const classes = useStyles();

  const [open, setOpen] = React.useState(false);

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const loadAllRestaurant = (): void => {
    RestaurantService.getRestauByLieux(selectedLieux).then((restaurant: React.SetStateAction<Restaurant[]>) =>
      setCurrentRestaurant(restaurant)
    );
    RestaurantService.getRestauByLieux(selectedLieux).then((restaurant: React.SetStateAction<Restaurant[]>) =>
      setDisplayedRestaurant(restaurant)
    );
  };
  useEffect(() => {
    loadAllRestaurant();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [selectedLieux, open]);

  const handleShuffle = (): void => {
    if (displayedRestaurant.length === 1) {
      handleReset();
    }
    const aChoice: Restaurant = currentRestaurant[Math.floor(Math.random() * currentRestaurant.length)];
    setDisplayedRestaurant([aChoice]);
  };

  const handleReset = (): void => {
    setDisplayedRestaurant(currentRestaurant);
  };

  const handleLieuxChange = (e: React.ChangeEvent<{ name?: string | undefined; value: unknown }>): void => {
    const value: any = e.target.value;
    setSelectedLieux(value);
  };

  return (
    <div>
      <Select
        variant="outlined"
        required
        style={{ marginTop: "5px" }}
        label="lieux"
        id="lieux"
        name="lieux"
        value={selectedLieux}
        className={classes.content}
        onChange={(e) => handleLieuxChange(e)}>
        {Object.values(LieuxRestau).map((value: string) => {
          return (
            <MenuItem key={value} value={value}>
              {value}
            </MenuItem>
          );
        })}
      </Select>
      <Grid container justify="center" item xs={7} spacing={1} className={classes.content}>
        {displayedRestaurant ? (
          displayedRestaurant.map((restaurant) => <RestaurantCard key={restaurant.id} restaurant={restaurant} />)
        ) : (
          <> </>
        )}
      </Grid>

      <Dialog open={open} onClose={handleClose}>
        <RestaurantAdd handleClose={handleClose} />
      </Dialog>
      <Button style={{ position: "fixed", bottom: "100px", right: "8%" }} onClick={handleOpen}>
        <AddCircleIcon fontSize="large" color="primary" />
      </Button>
      <Grid className={classes.button}>
        <Button onClick={handleReset}>
          {" "}
          <RestoreIcon fontSize="large" color="primary" />
        </Button>
        <Button onClick={handleShuffle}>
          {" "}
          <ShuffleIcon fontSize="large" color="primary" />
        </Button>
      </Grid>
    </div>
  );
};
export default RestaurantScreen;

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const useStyles = makeStyles((theme: Theme) => ({
  content: {
    marginLeft: "20%",
  },
  button: {
    [theme.breakpoints.down("xs")]: {
      display: "flex",
      flexDirection: "column",
    },
    bottom: "100px",
    position: "fixed",
    left: "4%",
  },
}));
