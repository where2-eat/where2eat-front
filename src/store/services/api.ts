import axios from "axios";

const API_URL = "http://199.247.11.72:8000/api/";


const api = axios.create({
  baseURL: API_URL,
  headers: {
    "Content-Type": "application/json",
  },
});

export default api;
