import api from "./api";
import Restaurant from "../../models/restaurant.model";

export default class RestaurantService {
  static getPublicRestau(): Promise<Restaurant[]> {
    return api
      .get("restau")
      .then((response) => {
        return response.data;
      })
      .catch((error) => this.handleError(error));
  }

  static getRestauByID(id: number): Promise<Restaurant> {
    return api
      .get(`restau/${id}`)
      .then((response) => {
        return response.data;
      })
      .catch((error) => this.handleError(error));
  }

  static getRestauByLieux(lieux: string): Promise<Restaurant[]> {
    return api
      .get(`restau/lieux/${lieux}`)
      .then((response) => {
        return response.data;
      })
      .catch((error) => this.handleError(error));
  }

  static deleteRestauByID(id: number): Promise<Restaurant> {
    return api
      .delete(`restau/${id}`)
      .then((response) => {
        return response.data;
      })
      .catch((error) => this.handleError(error));
  }

  static addRestaurant(restaurant: Restaurant): Promise<Restaurant> {
    return api
      .post(`restau`, {
        name: restaurant.name,
        type: restaurant.type,
        id: restaurant.id,
        link: restaurant.link,
        note: restaurant.note,
        lieux: restaurant.lieux,
      })
      .then((response) => {
        return response.data;
      })
      .catch((error) => this.handleError(error));
  }

  static updateRestaurant(restaurant: Restaurant): Promise<Restaurant> {
    return api
      .put(`restau/${restaurant.id}`, {
        name: restaurant.name,
        type: restaurant.type,
        link: restaurant.link,
        note: restaurant.note,
        lieux: restaurant.lieux,
      })
      .then((response) => {
        return response.data;
      })
      .catch((error) => this.handleError(error));
  }

  // eslint-disable-next-line @typescript-eslint/ban-types
  static isEmpty(data: Object): boolean {
    return Object.keys(data).length === 0;
  }

  static handleError(error: Error): void {
    console.error(error);
  }
}
